using System.Collections;
using UnityEngine;

public class ActionBlackScreen : MonoBehaviour
{
    public GameObject openScreen;
    public GameObject blackScreen;
    
    public void OffScreen()
    {
        gameObject.SetActive(false);
    }

    public void Restart()
    {
        blackScreen.SetActive(true);
        StartCoroutine(Coroutine());
    }

    IEnumerator Coroutine()
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
        blackScreen.SetActive(false);
        openScreen.SetActive(true);
    }
}
