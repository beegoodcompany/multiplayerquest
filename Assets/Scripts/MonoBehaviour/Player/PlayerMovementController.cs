using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class PlayerMovementController : NetworkBehaviour
{
    public float Speed;
    public float CameraSpeed;
    public GameObject PlayerObject;
    public Animator animator;

    private Rigidbody rigidbody;
    private GameObject CameraHolder;
    private Vector3 direction;

    public override void OnStartAuthority()
    {
        PlayerObject.SetActive(false);
        rigidbody = transform.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (CheckScene() == false)
        {
            return;
        }

        if (PlayerObject.activeSelf == false)
        {
            SetPosition();
            CameraHolder = GameObject.Find("Camera");
            var gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

            var playerPos = transform.position;
            var position = new Vector3(playerPos.x, 10, playerPos.z);
            CameraHolder.transform.position = position;
            StartCoroutine(Coroutine(gameManager));
            PlayerObject.SetActive(true);
        }
        
        if (isOwned)
        {
            Movement();
        }
    }

    private void LateUpdate()
    {
        if (CheckScene() == false)
        {
            return;
        }
        
        if (isOwned)
        {
            if (CameraHolder != null)
            {
                CameraMove();
            }
        }
    }

    private void CameraMove()
    {
        var playerPos = transform.position;
        var position = new Vector3(playerPos.x, 10, playerPos.z);
        
        CameraHolder.transform.position = Vector3.Lerp(CameraHolder.transform.position, position,
            CameraSpeed * Time.deltaTime);
    }
    
    private bool CheckScene()
    {
        return SceneManager.GetActiveScene().name == "TestGame";
    }

    private void Movement()
    {
        direction.x = Input.GetAxisRaw("Horizontal");
        direction.z = Input.GetAxisRaw("Vertical");

        
        animator.SetFloat("Horizontal", direction.x);
        animator.SetFloat("Vertical", direction.z);

        animator.SetBool("Move", direction != Vector3.zero);

        rigidbody.velocity = direction * Speed * Time.fixedDeltaTime;
    }
    
    private void SetPosition()
    {
        var steamLobby = GameObject.Find("Steam Manager").GetComponent<SteamLobby>();
        var pos1 = GameObject.Find("Pos1").transform.position;
        var pos2 = GameObject.Find("Pos2").transform.position;
        
        if (steamLobby.HostPlayer)
        {
            transform.position = pos2;
        }
        else
        {
            transform.position = pos1;
        }
    }

    IEnumerator Coroutine(GameManager gameManager)
    {
        yield return new WaitForSeconds(2f);
        gameManager.InitScene();
    }
}
