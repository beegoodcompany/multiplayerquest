using UnityEngine;

public class SpriteOrderController : MonoBehaviour
{
    public float offset = 0;
    private int sortingOrder = 0;
    private Renderer renderer;
    
    void Start() => renderer = transform.GetComponent<Renderer>();

    void LateUpdate()
    {
        renderer.sortingOrder = (int)(sortingOrder - transform.position.z + offset);
    }
}
