using UnityEngine;

public class SteamActivator : MonoBehaviour
{
    public GameObject Steam;
    public GameObject LogoWindow;
    
    public void EnableSteam()
    {
        LogoWindow.SetActive(false);
        Steam.SetActive(true);
    }
}
