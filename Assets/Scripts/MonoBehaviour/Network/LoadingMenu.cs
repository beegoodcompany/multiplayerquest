using UnityEngine;
using TMPro;

public class LoadingMenu : MonoBehaviour
{
    [SerializeField] private string message = "Connect to Steam Network";
    [SerializeField] private TextMeshProUGUI textLoad;

    public void Loading()
    {
        textLoad.text += ".";
    }

    public void LoadEnd()
    {
        textLoad.text = message;
    }
}
