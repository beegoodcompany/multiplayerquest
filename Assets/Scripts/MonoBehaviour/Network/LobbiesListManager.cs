using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class LobbiesListManager : MonoBehaviour
{
    public static LobbiesListManager instance;

    public GameObject lobbyDataItemPrefab;
    public GameObject lobbyListContent;

    public List<GameObject> listOfLobbies = new();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
    
    public void GetListOfLobbies()
    {
        SteamLobby.Instance.GetLobbyList();
    }
    
    public void DestroyLobbies()
    {
        foreach (var lobby in listOfLobbies)
        {
            Destroy(lobby);
        }

        listOfLobbies.Clear();
    }


    public void DisplayLobbies(List<CSteamID> lobbyIDs, LobbyDataUpdate_t result)
    {
        for (int i = 0; i < lobbyIDs.Count; i++)
        {
            if (lobbyIDs[i].m_SteamID == result.m_ulSteamIDLobby)
            {
                if (SteamMatchmaking.GetLobbyData((CSteamID)lobbyIDs[i].m_SteamID, "name") == "")
                {
                    continue;
                }
                
                GameObject createdItem = Instantiate(lobbyDataItemPrefab);

                createdItem.GetComponent<LobbyDataEntry>().lobbyID = (CSteamID)lobbyIDs[i].m_SteamID;

                createdItem.GetComponent<LobbyDataEntry>().lobbyName =
                    SteamMatchmaking.GetLobbyData((CSteamID)lobbyIDs[i].m_SteamID, "name");

                createdItem.GetComponent<LobbyDataEntry>().SetLobbyData();

                createdItem.transform.SetParent(lobbyListContent.transform);
                createdItem.transform.localScale = Vector3.one;

                listOfLobbies.Add(createdItem);
            }
        }
    }
}