using Steamworks;
using Mirror;
using UnityEngine;

public class PlayerObjectController : NetworkBehaviour
{
    [SyncVar] public int ConnectionID;
    [SyncVar] public ulong PlayerSteamID;
    [SyncVar(hook = nameof(PlayerNameUpdate))] public string PlayerName;
    [SyncVar(hook = nameof(PlayerReadyUpdate))] public bool Ready;

    private GameManager gameManager;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private CustomNetworkManager _manager;

    private CustomNetworkManager CheckManager
    {
        get
        {
            if (_manager != null)
            {
                return _manager;
            }

            return _manager = CustomNetworkManager.singleton as CustomNetworkManager;
        }
    }

    public override void OnStartAuthority()
    {
        CmdSetPlayerName(SteamFriends.GetPersonaName());
        gameObject.name = "LocalGamePlayer";
        LobbyController.Instance.FindLocalPlayer();
        LobbyController.Instance.UpdateLobbyName();
    }

    public override void OnStartClient()
    {
        CheckManager.GamePlayers.Add(this);
        LobbyController.Instance.UpdateLobbyName();
        LobbyController.Instance.UpdatePlayerList();
    }
    
    public override void OnStopClient()
    {
        CheckManager.GamePlayers.Remove(this);
        LobbyController.Instance.UpdatePlayerList();
    }

    [Command]
    private void CmdSetPlayerName(string playerName)
    {
        this.PlayerNameUpdate(this.PlayerName,playerName);
    }
    
    public void PlayerNameUpdate(string OldValue, string NewValue)
    {
        if (isServer)
        {
            this.PlayerName = NewValue;
        }

        if (isClient)
        {
            LobbyController.Instance.UpdatePlayerList();
        }
    }

    public void PlayerReadyUpdate(bool oldValue, bool newValue)
    {
        if (isServer)
        {
            this.Ready = newValue;
        }

        if (isClient)
        {
            LobbyController.Instance.UpdatePlayerList();
        }
    }

    [Command]
    private void CmdSetPlayerReady()
    {
        this.PlayerReadyUpdate(this.Ready,!this.Ready);
    }

    public void ChangeReady()
    {
        if (isOwned)
        {
            CmdSetPlayerReady();
        }
    }

    public void CanStartGame(string sceneName)
    {
        if (isOwned)
        {
            CmdCanStartGame(sceneName);
        }
    }
    
    [Command]
    private void CmdCanStartGame(string sceneName)
    {
        _manager.StartGame(sceneName);
    }

    private bool UpdateValue;
    
    [SyncVar(hook = nameof(SyncChange))] private bool _SyncChange;
    [SyncVar(hook = nameof(SyncFunction))] private string _SyncFunction;

    void SyncFunction(string oldValue, string newValue)
    {
        _SyncFunction = newValue;
    }
    
    void SyncChange(bool oldValue, bool newValue)
    {
        gameManager ??= GameObject.Find("GameManager").GetComponent<GameManager>();

        gameManager.SetCallFunction(_SyncFunction);
    }
    
    [Server]
    private void ChangeString(string newValue)
    {
        _SyncFunction = newValue;
    }
    
    [Command]
    private void CmdChangeString(string newValue)
    {
        ChangeString(newValue);
    }
    
    [Server]
    private void ChangeValue(bool newValue)
    {
        _SyncChange = newValue;
    }
    
    [Command]
    private void CmdChangeValue(bool newValue)
    {
        ChangeValue(newValue);
    }
    
    public void InitFunction(string str)
    {
        if (isServer)
        {
            ChangeString(str);
        }
        else
        {
            CmdChangeString(str);
        }
    }
    
    public void CallFunction()
    {
        UpdateValue = !UpdateValue;
        
        if (isServer)
        {
            ChangeValue(UpdateValue);
        }
        else
        {
            CmdChangeValue(UpdateValue);
        }
    }
}
