using System.Collections.Generic;
using System.Linq;
using Mirror.FizzySteam;
using UnityEngine;
using Steamworks;
using TMPro;
using UnityEngine.SceneManagement;

public class LobbyController : MonoBehaviour
{
    public static LobbyController Instance;

    [SerializeField] private TextMeshProUGUI LobbyNameText;
    [SerializeField] private TextMeshProUGUI Timer;
    [SerializeField] private PlayerListItem PlayerListItemPrefab;
    [SerializeField] private GameObject LocalPlayerObject;
    [SerializeField] private GameObject Content;
    private bool AllReady;
    private float time = 10;

    public ulong CurrentLobbyID;
    public bool PlayerItemCreated = false;
    private List<PlayerListItem> PlayerListItems = new();

    public PlayerObjectController LocalPlayerController;

    private CustomNetworkManager _manager;

    private CustomNetworkManager CheckManager
    {
        get
        {
            if (_manager != null)
            {
                return _manager;
            }

            return _manager = CustomNetworkManager.singleton as CustomNetworkManager;
        }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void ReadyPlayer()
    {
        LocalPlayerController.ChangeReady();
    }

    public void ChackIsAllReady()
    {
        var ready = 0;
        AllReady = false;

        foreach (var player in CheckManager.GamePlayers)
        {
            if (player.Ready)
            {
                ready += 1;
            }
        }

        if (ready > 1)
        {
            AllReady = true;
        }
    }
    
    public void UpdateLobbyName()
    {
        CurrentLobbyID = GameObject.Find("Steam Manager").GetComponent<SteamLobby>().CurrentLobbyID;
        LobbyNameText.text = SteamMatchmaking.GetLobbyData(new CSteamID(CurrentLobbyID), "name");
    }

    public void OpenFriends()
    {
        CurrentLobbyID = GameObject.Find("Steam Manager").GetComponent<SteamLobby>().CurrentLobbyID;
        SteamFriends.ActivateGameOverlayInviteDialog(new CSteamID(CurrentLobbyID));
    }
    
    public void UpdatePlayerList()
    {
        if (PlayerItemCreated == false)
        {
            CreateHostPlayerItem();
        }

        if (PlayerListItems.Count < CheckManager.GamePlayers.Count)
        {
            CreateClientPlayerItem();
        }
        
        if (PlayerListItems.Count > CheckManager.GamePlayers.Count)
        {
            RemovePlayerItem();
        }
        
        if (PlayerListItems.Count == CheckManager.GamePlayers.Count)
        {
            UpdatePlayerItem();
        }
    }

    public void FindLocalPlayer()
    {
        LocalPlayerObject = GameObject.Find("LocalGamePlayer");
        LocalPlayerController = LocalPlayerObject.GetComponent<PlayerObjectController>();
    }
    
    public void CreateHostPlayerItem()
    {
        foreach (var player in CheckManager.GamePlayers)
        {
            var playerItem = Instantiate(PlayerListItemPrefab);
            playerItem.PlayerName = player.PlayerName;
            playerItem.ConnectionID = player.ConnectionID;
            playerItem.PlayerSteamID = player.PlayerSteamID;
            playerItem.Ready = player.Ready;
            playerItem.SetPlayerValues();
            playerItem.transform.SetParent(Content.transform);
            
            PlayerListItems.Add(playerItem);
        }

        PlayerItemCreated = true;
    }

    public void CreateClientPlayerItem()
    {
        foreach (var player in CheckManager.GamePlayers)
        {
            if (!PlayerListItems.Any(b => b.ConnectionID == player.ConnectionID))
            {
                var playerItem = Instantiate(PlayerListItemPrefab);
                playerItem.PlayerName = player.PlayerName;
                playerItem.ConnectionID = player.ConnectionID;
                playerItem.PlayerSteamID = player.PlayerSteamID;
                playerItem.Ready = player.Ready;
                playerItem.SetPlayerValues();
                playerItem.transform.SetParent(Content.transform);
                
                PlayerListItems.Add(playerItem);
            }
        }
    }

    public void UpdatePlayerItem()
    {
        foreach (var player in CheckManager.GamePlayers)
        {
            foreach (var PlayerListItemScript in PlayerListItems)
            {
                if (PlayerListItemScript.ConnectionID == player.ConnectionID)
                {
                    PlayerListItemScript.PlayerName = player.PlayerName;
                    PlayerListItemScript.Ready = player.Ready;
                    PlayerListItemScript.SetPlayerValues();
                }
            }
        }
        
        ChackIsAllReady();
    }
    
    public void RemovePlayerItem()
    {
        List<PlayerListItem> playerListItemsToRemove = new();

        foreach (var playerListItem in PlayerListItems)
        {
            if (!CheckManager.GamePlayers.Any(b => b.ConnectionID == playerListItem.ConnectionID))
            {
                playerListItemsToRemove.Add(playerListItem);
            }
        }

        if (playerListItemsToRemove.Count > 0)
        {
            foreach (var playerToRemove in playerListItemsToRemove)
            {
                GameObject objectToRemove = playerToRemove.gameObject;
                PlayerListItems.Remove(playerToRemove);
                Destroy(objectToRemove);
            }
        }
    }

    private void Update()
    {
        if (AllReady == false)
        {
            Timer.text = "";
            time = 10;
            return;
        }

        time -= Time.deltaTime;
        
        
        if (time < 0.2)
        {
            LocalPlayerController.CanStartGame("TestGame");
            return;
        }

        var sec = (int)time;
        Timer.text = sec.ToString();
    }
    
    public void ExitToMenu()
    {
        var fizzy = GameObject.Find("Network Manager").GetComponent<FizzySteamworks>();
        var steamLobby = GameObject.Find("Steam Manager").GetComponent<SteamLobby>();
        fizzy.ClientDisconnect();
        fizzy.Shutdown();

        if (steamLobby.HostPlayer)
        {
            _manager.StopHost();
        }
        else
        {
            _manager.StopClient();
        }

        SteamMatchmaking.LeaveLobby((CSteamID)CurrentLobbyID);
        SceneManager.LoadScene("MainMenu");
    }
}
