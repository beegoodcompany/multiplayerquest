using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class GameManager
{
    private PlayerSide playerSide;
    private PlayerObjectController player;
    private Dictionary<string, Action> clientCallActions = new();
    
    public void InitScene() => player = GameObject.Find("LocalGamePlayer").gameObject.GetComponent<PlayerObjectController>();
    public void AddCallAction(string name, Action action) => clientCallActions.Add(name, action);
    public void SetPlayerSide(PlayerSide side) => playerSide = side;
    //public void OnFunction(string str) => StartCoroutine(DelayFunction(str));
    //public void OnPlayerFunction(string str) => StartCoroutine(DelayFunction($"{str}_{playerSide}"));

    //TODO: FIX THIS BUG
    private IEnumerator DelayFunction(string str)
    {
        player.InitFunction(str);
        yield return new WaitForSeconds(0.2f);
        player.CallFunction();
    }

    public void SetCallFunction(string str)
    {
        if (clientCallActions.TryGetValue(str, out Action action) == false)
        {
            return;
        }
        
        action.Invoke();
    }
}
