using UnityEngine;
using Steamworks;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private PlayerListItem PlayerInfo;
    [SerializeField] private GameObject LoadingWindow;
    private SteamLobby steamLobby;
    
    void Start()
    {
        var manager = GameObject.Find("Steam Manager");

        if (manager == null)
        {
            return;
        }
        
        steamLobby = manager.GetComponent<SteamLobby>();
        
        PlayerInfo.PlayerNameText.text = SteamFriends.GetPersonaName();
        var index = SteamFriends.GetLargeFriendAvatar(SteamUser.GetSteamID());
        if (index == -1)
        {
            return;
        }

        PlayerInfo.PlayerIcon.texture = GetSteamImageAsTexture(index);
        
        LoadingWindow.SetActive(false);
    }
    
    public void Host()
    {
        steamLobby.HostLobby();
    }
    
    private Texture2D GetSteamImageAsTexture(int iImage)
    {
        Texture2D texture = null;

        bool isValid = SteamUtils.GetImageSize(iImage, out uint width, out uint height);
        if (isValid)
        {
            byte[] image = new byte[width * height * 4];

            isValid = SteamUtils.GetImageRGBA(iImage, image, (int)(width * height * 4));

            if (isValid)
            {
                texture = new Texture2D((int)width, (int)height, TextureFormat.RGBA32, false, true);
                texture.LoadRawTextureData(image);
                texture.Apply();
            }
        }
        return texture;
    }
}
